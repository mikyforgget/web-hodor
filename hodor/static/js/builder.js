$(function () {
    $.fn.rm_effects = function () {
        return $(this).removeClass(function (index, css) {
            return (css.match(/\b_\S+/g) || []).join(' ');
        });
    };

    function nf0(n) {
        return Math.round(n).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\xa0");
    }

    function r2(n) {
        return n.toFixed(2)
    }

    function nf2(n) {
        return r2(n).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "\xa0")
    }

    function ini_class(ini) {
        if (ini < 3) {
            return '_c702020';
        }
        if (ini < 7) {
            return '_c602020';
        }
        if (ini < 11) {
            return '_c502020';
        }
        if (ini < 15) {
            return '_c402020';
        }
        if (ini < 18) {
            return '_c302020';
        }
        if (ini < 22) {
            return '_c202020';
        }
        if (ini < 26) {
            return '_c203020';
        }
        if (ini < 30) {
            return '_c204020';
        }
        if (ini < 34) {
            return '_c205020';
        }
        if (ini < 37) {
            return '_c206020';
        }
        return '_c207020'
    }

    function addRemoveGenericRows(target, newRowCallback) {
        // basically the goal is to keep 'exactly one' blank row
        let last = null;
        let reversedLis = target.get().reverse();

        if (reversedLis.length === 0) {
            // no rows
            newRowCallback();
            return;
        }

        if (parseInt($(reversedLis[0]).find('.input-selector option:selected').val()) !== 0) {
            // last row ain't an empty
            newRowCallback();
            return;
        }

        reversedLis.some(function (li) {
            let unitId = parseInt($(li).find('.input-selector option:selected').val());

            if (unitId > 0) return true;

            if (last !== null) {
                last.remove();
            }
            last = $(li);

            return false;
        });
    }

    $.getJSON(URL_AJAX_UNITS, function (data) {
        const ALL_UNITS_SORTED_BY_NAME = data['units'];

        const ALL_UNITS_BY_ID = {};
        ALL_UNITS_SORTED_BY_NAME.forEach(function (unit) {
            ALL_UNITS_BY_ID[unit.unit_id] = unit;
        });
        const FLIER_CN = {
            'P': 'Pozemní',
            'L': 'Letecká',
        }
        const ATTACK_CN = {
            'B': 'Bojová',
            'S': 'Střelecká',
        }

        ALL_UNITS_SORTED_BY_NAME.forEach(function (unit) {
            $('<option>')
                .val(unit.unit_id)
                .data('unit-name', unit.name)
                .text(unit.name + ' [' + unit.initiative + '] ' + unit.movement_type + unit.attack_type + (unit.movement > 1 ? unit.movement : ''))
                .appendTo($('#template-unit .builder-row .unit'));
        });

        function add_empty_unit_row() {
            return $('#template-unit tr').clone(true, true).appendTo($('#units tbody'));
        }

        function refresh_rows(refresh_result) {
            let unit_count = 0;
            let total_count = 0;
            let total_power = 0;
            let total_recruit_tu = 0;
            let total_upkeep_gp = 0;
            let total_upkeep_gp_real = 0;
            let total_upkeep_mn = 0;
            let total_upkeep_pp = 0;
            let result_text = '';
            let input = [];
            let row_count = 0;

            $('.builder-row').each(function () {
                let row = $(this);

                let unit_id = row.find('.unit').val();
                let count = parseInt(row.find('.count').val());
                let xp = parseInt(row.find('.xp').val());
                let target = row.find('.target').val();

                row.find('.flier').rm_effects().text('');
                row.find('.attack').rm_effects().text('');
                row.find('.movement').rm_effects().text('');
                row.find('.initiative').rm_effects().text('');
                row.find('.color').rm_effects().text('');
                row.find('.power').rm_effects().text('');
                row.find('.total_power').rm_effects().text('');
                row.find('.recruit_per_tu').rm_effects().text('');
                row.find('.recruit_tu').val('');
                row.find('.gp').rm_effects().text('');
                row.find('.gp_real').rm_effects().text('');
                row.find('.mn').rm_effects().text('');
                row.find('.pp').rm_effects().text('');

                row_count += 1;
                row.find('.counter').text(row_count);

                if (unit_id in ALL_UNITS_BY_ID) {
                    input.push({
                        'unit_id': unit_id,
                        'count': count,
                        'xp': xp,
                        'target': target,
                    });

                    let unit = ALL_UNITS_BY_ID[unit_id];
                    let power = Math.round(unit.power * count * xp / 100.0);
                    let upkeep_gp = Math.round(-unit.upkeep_gold * count);
                    let upkeep_gp_real = Math.round(-(unit.upkeep_gold + 0.00545 * 3) * count);
                    let upkeep_mn = Math.round(-unit.upkeep_mana * count);
                    let upkeep_pp = Math.round(-unit.upkeep_pop * count);

                    unit_count++;
                    total_count += count;
                    total_power += power;
                    total_upkeep_gp += upkeep_gp;
                    total_upkeep_gp_real += upkeep_gp_real;
                    total_upkeep_mn += upkeep_mn;
                    total_upkeep_pp += upkeep_pp;

                    row.find('.flier').addClass('_' + unit.movement_type).text(FLIER_CN[unit.movement_type]);
                    row.find('.attack').addClass('_' + unit.attack_type).text(ATTACK_CN[unit.attack_type]);
                    row.find('.movement').text(unit.movement);
                    row.find('.initiative').rm_effects().addClass(ini_class(unit.initiative)).text(unit.initiative);
                    row.find('.color').addClass('_unit-color-' + unit.color).text(unit.color + unit.specialization);
                    row.find('.power').text(unit.power);
                    row.find('.total_power').text(nf0(power));

                    if (unit.total_recruit_tu > 0) {
                        let recruit_tu = Math.ceil(count / unit.total_recruit_tu);
                        total_recruit_tu += recruit_tu;
                        row.find('.recruit_tu').val(recruit_tu);
                        row.find('.recruit_per_tu').text(nf2(unit.total_recruit_tu));
                    }

                    row.find('.gp').rm_effects().text(nf0(upkeep_gp));
                    row.find('.gp_real').rm_effects().text(nf0(upkeep_gp_real));
                    row.find('.mn').rm_effects().text(nf0(upkeep_mn));
                    row.find('.pp').rm_effects().text(nf0(upkeep_pp));

                    result_text += unit.name + ";" + xp / 100 + ";" + count + (target ? ";" + target : "") + "\n";
                }
            });

            $('.total .unit_count').text(nf0(unit_count));
            $('.total .total_count').text(nf0(total_count));
            $('.total .total_power').text(nf0(total_power));
            $('.total .total_recruit_tu').text(nf0(total_recruit_tu));
            $('.total .total_upkeep_gp').text(nf0(total_upkeep_gp));
            $('.total .total_upkeep_gp_real').text(nf0(total_upkeep_gp_real));
            $('.total .total_upkeep_mn').text(nf0(total_upkeep_mn));
            $('.total .total_upkeep_pp').text(nf0(total_upkeep_pp));
            addRemoveGenericRows($('#units tbody .builder-row'), add_empty_unit_row);


            if (refresh_result) {
                $('.result_text')
                    .val(result_text);
            }
            window.localStorage.setItem("input", JSON.stringify(input));
        }


        function init_from_local_storage() {
            let input_json = window.localStorage.getItem('input');
            if (input_json === null) {
                input_json = '[]';
            }

            let input = JSON.parse(input_json);

            input.forEach(function (unit) {
                let row = add_empty_unit_row();

                row.find('.unit').val(unit['unit_id']);
                row.find('.count').val(unit['count']);
                row.find('.xp').val(unit['xp']);
                row.find('.target').val(unit['target']);
            });

            add_empty_unit_row();
            refresh_rows(true);
        }

        $(document).on('change', '.change-trigger', function () {
            refresh_rows(true);
        });
        // noinspection JSJQueryEfficiency
        $('.result_text').on('change', function () {
            $('#units tbody .builder-row').remove();
            $(this).val().split("\n").forEach(function (row) {
                let parts = row.split(";");
                if (parts.length >= 3) {
                    let row = add_empty_unit_row();

                    row.find('.unit option').filter(function () {
                        return $(this).data('unit-name') === parts[0]
                    }).prop('selected', true);
                    row.find('.xp').val(Math.round(parseFloat(parts[1]) * 100));
                    row.find('.count').val(parts[2]);
                    row.find('.target').val(parts[3]);
                }
            });

            refresh_rows(false);
        })

        $('#reset').click(function () {
            $('#units tbody .builder-row').remove();
            refresh_rows(true);
        });

        $('.recruit_tu').change(function () {
            let tu = Math.max(1, parseInt($(this).val()));
            let row = $(this).parents('.builder-row');
            let unit_id = row.find('.unit').val();

            if (unit_id in ALL_UNITS_BY_ID) {
                let unit = ALL_UNITS_BY_ID[unit_id];

                if (unit.total_recruit_tu > 0) {
                    let models = Math.floor(unit.total_recruit_tu * tu);
                    row.find('.count').val(models);

                    refresh_rows(true);
                }
            }
        });

        init_from_local_storage();
    });
});
