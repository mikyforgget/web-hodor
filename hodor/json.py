from django.core.serializers.json import DjangoJSONEncoder

from hodor.data.provider import UnitFullInfo


class UnitFullInfoEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, UnitFullInfo):
            return obj.__dict__
        return super().default(obj)
