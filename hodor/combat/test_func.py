import doctest


def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite('hodor.combat.func'))
    return tests
