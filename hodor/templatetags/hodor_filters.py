from datetime import datetime
from typing import Optional

import markdown
from django import template
from django.conf import settings
from django.utils import timezone
from django.utils.safestring import mark_safe

from hodor.combat.func import AREA_20K_MUL
from hodor.combat.logevent import LogEvent, GateAttacked, LogUnit
from hodor.models import TournamentResult

register = template.Library()


@register.filter
def thousand_format(number):
    if number is None or number == '':
        return mark_safe('&mdash;')

    return mark_safe(f'{number:,}'.replace(',', '&nbsp;'))


@register.filter
def percent(number, decimals=1):
    if number is None:
        return None
    f = '%%.%df%%%%' % decimals
    return f % (number * 100)


@register.filter
def pm_percent(number, decimals=1):
    if number > 0:
        f = '+%%.%df%%%%' % decimals
    else:
        f = '%%.%df%%%%' % decimals
    return (f % (number * 100)).replace('-', '−')


@register.filter
def pm_int_thousands(number, thousand_separator='&nbsp;'):
    if number is None:
        return None

    number = int(number)
    if number > 0:
        base = f'+{int(number):,}'
    else:
        base = f'{int(number):,}'

    return mark_safe(base.replace(',', thousand_separator).replace('-', '−'))


@register.filter
def long_time(value: Optional[datetime]) -> str:
    """
    Returns date/time in long format 'd.m. HH:mm'.

    Uses non-retarded format (sorry US).
    """
    if value is None:
        return ''

    return '{d.day}.{d.month}. {d.hour:02}:{d.minute:02}'.format(d=timezone.get_current_timezone().normalize(value))


@register.inclusion_tag('hodor/include/render_battle_log_entry.html')
def render_battle_log_entry(event: LogEvent):
    return {
        'event': event,
        'type': event.__class__.__name__,
    }


@register.inclusion_tag('hodor/include/render_log_unit.html')
def render_log_unit(unit: LogUnit):
    return {
        'unit': unit,
    }


@register.filter
def gate_defender_name(gate_number):
    return ('Klotho', 'Lachesis', 'Atropos', 'Caerme', 'Argo', 'Furia', 'Celesta', 'Tara')[gate_number - 1]


@register.filter
def nft(number: int, thousand_separator=' ') -> str:
    if not isinstance(number, int):
        # noinspection PyTypeChecker
        return number
    return '{:,}'.format(number).replace(',', thousand_separator)


@register.filter
def battle_log_entry(entry: LogEvent) -> str:
    if isinstance(entry, GateAttacked):
        return 'GA'

    return entry.as_ascii()


@register.filter
def mul(a, b) -> Optional[float]:
    if a is None:
        return None
    return a * b


@register.filter
def div(a, b) -> float:
    return a / b


@register.filter
def sub(a, b) -> float:
    return a - b


@register.filter
def add_f(a, b) -> float:
    return a + b


@register.filter('int')
def int_(number) -> int:
    return int(number)


@register.simple_tag
def tournament_result(tournament_results,
                      attacker_submission_id: int,
                      defender_submission_id: int) -> Optional[TournamentResult]:
    if attacker_submission_id not in tournament_results:
        return None

    if defender_submission_id not in tournament_results[attacker_submission_id]:
        return None

    return tournament_results[attacker_submission_id][defender_submission_id]


@register.filter
def tournament_result_bg(result: TournamentResult, negate: bool = False):
    diff = result.defender_losses - result.attacker_losses
    if negate:
        diff *= -1
    if diff > 0.02:
        return 'bg-success'

    if diff < -0.02:
        return 'bg-danger'

    return 'bg-secondary'


@register.simple_tag
def get_from_settings(key, default=None):
    return getattr(settings, key, default)


@register.filter
def nf_1(value: Optional[float], decimal_separator: str = ',') -> Optional[str]:
    if value is None:
        return None

    return f'{value:,.1f}'.replace(',', ' ').replace('.', decimal_separator)


@register.filter
def render_log_unit(unit: LogUnit):
    if unit.count_before_attack > 1:
        return '%d x %s' % (unit.count_before_attack, unit.name)

    return unit.name


@register.filter('markdown')
def markdown_(text: str):
    return mark_safe(markdown.markdown(text))


@register.filter
def har20k(coefficient: float) -> float:
    return coefficient * AREA_20K_MUL
