import logging
from typing import List, Optional, Iterable

from django.conf import settings
from django.core.cache import caches
from django.utils.text import slugify

from hodor.combat.combatmodel import UnitStatic, UnitMetaType
from hodor.data.static_fallback import fallback_static_units_by_id, fallback_static_units_by_name
from hodor.models import Jednotky, Budovy, Kouzla

UNITS_PROVIDER_CACHE_NAME = 'meliorannis_db_records'
COUNTERSPELL_ID = 42
SUPPORTED_SPELLS = (1001, 1002, 1004,
                    1501,
                    2001, 2002, 2004, 2007,
                    2507,
                    3001, 3003, 3010,
                    3501,
                    4001, 4002, 4005,
                    4501,
                    5002, 5006, 5010,
                    5501,
                    6700,
                    7001, 7002,
                    7502,
                    )

CLASSES = {
    '0B': 'Neutrální',
    'MM': 'Mág',
    'MB': 'Alchymista',
    'BB': 'Válečník',
    'BM': 'Klerik',
    'ZB': 'Hraničář',
    'ZM': 'Druid',
    'CM': 'Nekromant',
    'CB': 'Theurg',
    'SM': 'Iluzionista',
    'SB': 'Barbar',
    'FM': 'Vědma',
    'FB': 'Amazonka',
}

logger = logging.getLogger(__name__)


class UnitFullInfo:
    ATTACK_TYPE_RANGED = 'S'
    ATTACK_TYPE_MELEE = 'S'

    def __init__(self,
                 unit_id: int,
                 name: str,
                 damage: float,
                 armor: float,
                 life: float,
                 initiative: int,
                 attack_type: str,
                 movement_type: str,
                 movement: int,
                 power: float,
                 color: str,
                 specialization: str,
                 upkeep_gold: float,
                 upkeep_mana: float,
                 upkeep_pop: float,
                 total_recruit_tu: Optional[float],
                 ):
        self.unit_id = unit_id
        self.name = name
        self.damage = damage
        self.armor = armor
        self.life = life
        self.initiative = initiative
        self.attack_type = attack_type
        self.movement_type = movement_type
        self.movement = movement
        self.power = power
        self.color = color
        self.specialization = specialization
        self.upkeep_gold = upkeep_gold
        self.upkeep_mana = upkeep_mana
        self.upkeep_pop = upkeep_pop
        self.total_recruit_tu = total_recruit_tu

    def __str__(self) -> str:
        return '%s (%d)' % (self.name, self.unit_id)


class SpellFullInfo:
    def __init__(self,
                 spell_id: int,
                 name: str,
                 text_template: str,
                 color: str,
                 specialization: str,
                 min_tu: int,
                 max_tu: int,
                 gate: int,
                 spell_power_magic: int,
                 spell_power_combat: int,
                 ) -> None:
        self.spell_id = spell_id
        self.name = name
        self.text_template = text_template
        self.color = color
        self.specialization = specialization
        self.min_tu = min_tu
        """
        0-based
        """
        self.max_tu = max_tu
        """
        0-based
        """
        self.gate = gate
        self.spell_power_magic = spell_power_magic
        self.spell_power_combat = spell_power_combat

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return self.name

    @staticmethod
    def from_db_spell(db_spell: Kouzla) -> 'SpellFullInfo':
        return SpellFullInfo(
            spell_id=db_spell.id,
            name=db_spell.nazev,
            text_template=db_spell.text_pri_seslani,
            color=db_spell.barva,
            specialization=db_spell.special,
            min_tu=db_spell.sesl_tu - 1,
            max_tu=db_spell.sesl_tu_posl - 1,
            gate=db_spell.brana,
            spell_power_magic=db_spell.skm,
            spell_power_combat=db_spell.skb,
        )


class SpellGroupInfo:
    def __init__(self, key: str, label: str) -> None:
        self.key = key
        self.label = label
        self.spells = []  # type: List[SpellFullInfo]

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return '%s: %s' % (self.label, self.spells)


def static_unit_by_id(unit_id: int) -> UnitStatic:
    if settings.FORCE_STATIC_FALLBACK:
        return fallback_static_units_by_id[unit_id]

    cache = caches[UNITS_PROVIDER_CACHE_NAME]
    key = 'static_unit_by_id:%d' % unit_id

    unit = cache.get(key)
    if unit is None:
        db_unit = Jednotky.objects.get(id=unit_id)

        unit = UnitStatic(
            unit_id=db_unit.id,
            jmeno=db_unit.jmeno,
            dmg=db_unit.dmg,
            brneni=db_unit.brneni,
            zivoty=db_unit.zivoty,
            iniciativa=db_unit.iniciativa,
            typ_ut=db_unit.typ_ut,
            pohybl=db_unit.pohybl,
            power=db_unit.power,
            druh=db_unit.druh,
            iq=db_unit.iq,
            meta_type=_unit_meta_type(db_unit.id),
            rod=db_unit.rod,
        )

        logger.debug("Loaded unit %s from DB by id %d", unit.jmeno, unit_id)

        cache.set(key, unit)

    return unit


def static_unit_by_name(unit_name: str) -> UnitStatic:
    if settings.FORCE_STATIC_FALLBACK:
        return fallback_static_units_by_name[unit_name]

    cache = caches[UNITS_PROVIDER_CACHE_NAME]
    key = 'unit_by_name:%s' % unit_name

    unit = cache.get(key)
    if unit is None:
        db_unit = Jednotky.objects.filter(id__gte=1000, id__lte=7000).get(jmeno=unit_name)

        unit = UnitStatic(
            unit_id=db_unit.id,
            jmeno=db_unit.jmeno,
            dmg=db_unit.dmg,
            brneni=db_unit.brneni,
            zivoty=db_unit.zivoty,
            iniciativa=db_unit.iniciativa,
            typ_ut=db_unit.typ_ut,
            pohybl=db_unit.pohybl,
            power=db_unit.power,
            druh=db_unit.druh,
            iq=db_unit.iq,
            meta_type=_unit_meta_type(db_unit.id),
            rod=db_unit.rod,
        )

        logger.debug("Loaded unit %s from DB by name %s", unit.jmeno, unit_name)

        cache.set(key, unit)

    return unit


def combat_spell(spell_id: int) -> SpellFullInfo:
    if settings.FORCE_STATIC_FALLBACK:
        raise Exception("Unavailable when working from static cache")

    cache = caches[UNITS_PROVIDER_CACHE_NAME]
    key = 'spell_by_id:%d' % spell_id

    spell = cache.get(key)
    if spell is None:
        if spell_id == COUNTERSPELL_ID:
            spell = _counterspell()
        else:
            db_spell = Kouzla.objects.filter(id__in=SUPPORTED_SPELLS).get(id=spell_id)

            spell = SpellFullInfo.from_db_spell(db_spell)

            logger.debug("Loaded spell %s from DB by id %s", spell, spell_id)

        cache.set(key, spell)

    return spell


def all_combat_spells() -> List[SpellFullInfo]:
    if settings.FORCE_STATIC_FALLBACK:
        raise Exception("Unavailable when working from static cache")

    cache = caches[UNITS_PROVIDER_CACHE_NAME]
    key = 'all_combat_spells'
    spells = cache.get(key)
    if spells is None:
        logger.info("Loaded all spells from database")
        db_spells = Kouzla.objects.filter(id__in=SUPPORTED_SPELLS).order_by('nazev', 'id')

        spells = [_counterspell()]
        for db_spell in db_spells:
            spells.append(SpellFullInfo.from_db_spell(db_spell))

        spells.sort(key=lambda spell: slugify(spell.name))

        cache.set(key, spells)

    return spells


def all_combat_spell_groups() -> Iterable[SpellGroupInfo]:
    if settings.FORCE_STATIC_FALLBACK:
        raise Exception("Unavailable when working from static cache")

    combat_spells = all_combat_spells()
    combat_spells_by_groups = {}
    for spell in combat_spells:
        if spell.color == '0':
            keys = ['%s%s' % (spell.color, spell.specialization)]
        else:
            keys = [
                '%s%s' % (spell.color, 'B'),
                '%s%s' % (spell.color, 'M'),
            ]

        for key in keys:
            if key not in combat_spells_by_groups:
                combat_spells_by_groups[key] = SpellGroupInfo(key, CLASSES[key])
            combat_spells_by_groups[key].spells.append(spell)

    return sorted(combat_spells_by_groups.values(), key=lambda group: group.key)


def _counterspell() -> SpellFullInfo:
    return SpellFullInfo(
        spell_id=COUNTERSPELL_ID,
        name='Counterspell',
        text_template='Soupeřovo kouzlo xxKOUZLOxx bylo zrušeno.',
        color='0',
        specialization='B',
        min_tu=0,
        max_tu=3,
        gate=0,
        spell_power_magic=0,
        spell_power_combat=0,
    )


def unit_full_info_by_id(unit_id: int) -> UnitFullInfo:
    if settings.FORCE_STATIC_FALLBACK:
        raise Exception("Unavailable when working from static cache")

    cache = caches[UNITS_PROVIDER_CACHE_NAME]
    key = 'unit_full_info_by_id:%d' % unit_id

    unit = cache.get(key)
    if unit is None:
        unit = next(unit for unit in all_regular_units() if unit.unit_id == unit_id)

        cache.set(key, unit)

    return unit


def all_regular_units() -> List[UnitFullInfo]:
    if settings.FORCE_STATIC_FALLBACK:
        raise Exception("Unavailable when working from static cache")

    cache = caches[UNITS_PROVIDER_CACHE_NAME]
    key = 'all_regular_units_full_info'
    units = cache.get(key)
    if units is None:
        logger.info("Loaded all units and buildings from database for UnitFullInfo")

        buildings = Budovy.objects.all()
        buildings_max_by_id = {}
        for building in buildings:
            if building.max.isdigit():
                buildings_max_by_id[building.id] = int(building.max)

        db_units = Jednotky.objects.filter(id__gte=1000, id__lte=7000).order_by('id').all()
        units = []
        for db_unit in db_units:
            total_recruit_tu = None
            if db_unit.rekrut_bud in buildings_max_by_id:
                # force 2 decimals, as 0.36 * 15 = 5.3999999999999995. Could be decimal, but as we don't have the values in db as string, does not make sense
                total_recruit_tu = round(buildings_max_by_id[db_unit.rekrut_bud] * db_unit.jedn_za_tu * 100) / 100

            units.append(
                UnitFullInfo(
                    db_unit.id, db_unit.jmeno,
                    db_unit.dmg, db_unit.brneni, db_unit.zivoty, db_unit.iniciativa,
                    db_unit.typ_ut, db_unit.druh, db_unit.pohybl,
                    db_unit.power, db_unit.barva, db_unit.specializace,
                    db_unit.plat_z, db_unit.plat_m, db_unit.plat_l,
                    total_recruit_tu,
                )
            )

        units.sort(key=lambda unit: slugify(unit.name))
        cache.set(key, units)

    return units


def _unit_meta_type(unit_id: int) -> UnitMetaType:
    if 400 <= unit_id <= 403:
        return UnitMetaType.MILITIA

    if 1000 <= unit_id <= 7000:
        return UnitMetaType.REGULAR

    return UnitMetaType.SPECIAL
