from webhodor.settings.base import *

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)
INTERNAL_IPS = ['127.0.0.1', ]
ALLOWED_HOSTS = ['*']

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

ENABLE_DEBUG_TOOLBAR = False

if ENABLE_DEBUG_TOOLBAR:
    INSTALLED_APPS += ['debug_toolbar', ]
    MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

# default to True on local
WEB_HODOR_ALLOW_PRECISE_SIMULATION = env.bool('WEB_HODOR_ALLOW_PRECISE_SIMULATION', True)
