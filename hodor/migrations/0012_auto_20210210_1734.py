# Generated by Django 3.1.2 on 2021-02-10 16:34

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('hodor', '0011_tournamentsubmission_recruit_tu'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='result_type',
            field=models.CharField(choices=[('number_of_wins', 'NUMBER_OF_WINS'), ('area_taken', 'AREA_TAKEN')], default='number_of_wins', max_length=128),
        ),
        migrations.AddField(
            model_name='tournamentsubmission',
            name='area_taken',
            field=models.FloatField(default=0.0),
        ),
    ]
