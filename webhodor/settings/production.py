# noinspection PyUnresolvedReferences
from webhodor.settings.base import *

# TODO fix
ALLOWED_HOSTS = [
    '*',
]

if 'django.middleware.security.SecurityMiddleware' in MIDDLEWARE:
    MIDDLEWARE.insert(MIDDLEWARE.index('django.middleware.security.SecurityMiddleware') + 1,
                      'whitenoise.middleware.WhiteNoiseMiddleware')
else:
    MIDDLEWARE.insert(0, 'whitenoise.middleware.WhiteNoiseMiddleware')

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

DATABASES = {
    'default': env.db('DATABASE_URL', default='sqlite:///%s' % (BASE_DIR / 'db.prod.sqlite3')),
    'meliorannis': env.db('DATABASE_URL_MA', default='sqlite:///%s' % (BASE_DIR / 'db.meliorannis.sqlite3')),
}

DEBUG = False
ENABLE_GA_TAG = True
