FROM python:3
ENV PYTHONUNBUFFERED=1
# go with local for static files serving (really, this dockerfile is just a plaything)
ENV DJANGO_SETTINGS_MODULE=webhodor.settings.local

RUN mkdir /code
WORKDIR /code
COPY requirements /requirements/
RUN pip install -r /requirements/prod.txt

COPY . /code/

EXPOSE 8000
RUN python manage.py migrate
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
