# -*- coding: utf-8 -*-

from enum import Enum
from typing import Optional


class UnitMetaType(Enum):
    REGULAR = 1
    """Can be taken into battle as attacker"""

    SPECIAL = 2
    """Cannot be taken to battle"""

    MILITIA = 3
    """Defense of empty province"""


class UnitTargeting:
    COUNT = 'pocet'

    DAMAGE = 'dmg'

    LIFE = 'zivoty'

    INITIATIVE = 'iniciativa'


class UnitStatic:
    def __init__(self,
                 unit_id: int,
                 jmeno: str,
                 dmg: float,
                 brneni: float,
                 zivoty: float,
                 iniciativa: int,
                 typ_ut: str,
                 pohybl: int,
                 power: float,
                 druh: str,
                 iq: int,
                 meta_type: UnitMetaType,
                 rod: Optional[int] = None,
                 ):
        self.id = unit_id
        self.jmeno = jmeno
        self.dmg = dmg
        self.brneni = brneni
        self.zivoty = zivoty
        self.iniciativa = iniciativa
        self.typ_ut = typ_ut
        self.pohybl = pohybl
        self.power = power
        self.druh = druh
        self.iq = iq
        self.meta_type = meta_type
        self.rod = rod


class NewUnitAtGate:

    def __init__(self, unit_id: int, percent_power: Optional[float] = None, count: Optional[int] = None) -> None:
        self.unit_id = unit_id
        self.percent_power = percent_power
        self.count = count


class UserInput:

    def __init__(self,
                 name: str = None,
                 experience: float = 0.4,
                 count: int = None,
                 power: int = None,
                 unit_id: int = None,
                 targeting: str = None,
                 is_support: bool = False,
                 ):
        self.name = name
        self.unit_id = unit_id
        self.experience = experience
        self.count = count
        self.power = power
        self.targeting = targeting
        self.is_support = is_support


class Unit:
    def __init__(self,
                 is_attacker_player: bool,
                 id_jedn: int,
                 nazev: str,
                 pocet: int,
                 protiutok_poc: int,
                 dmg: float,
                 brneni_celkem: int,
                 zivoty: float,
                 iniciativa: float,
                 typ_ut: str,
                 pohybl: int,
                 power: float,
                 power_at_full_xp: float,
                 druh: str,
                 celkem_zivotu: float,
                 podle: str,
                 zkusenost: float):
        self.is_attacker_player = is_attacker_player
        self.id_jedn = id_jedn
        self.nazev = nazev

        self.pocet = pocet
        self.pocet_puvodni = pocet

        self.protiutok_poc = protiutok_poc
        self.protiutok_poc_puvodni = protiutok_poc

        self.dmg = dmg
        """
        Damage as in DB column is used only for targeting. NOTE that in perl code, dmg_z is stored in dmg variable!!
        """
        self.dmg_z = dmg
        """
        This is the amount of damage a unit actually does, except for targeting!
        """

        self.brneni_celkem = brneni_celkem
        self.brneni_celkem_puvodni = brneni_celkem

        self.zivoty = zivoty

        self.iniciativa = iniciativa
        self.iniciativa_z = iniciativa
        """
        iniciativa_z is for the turn, iniciativa is fixed value.
        """

        self.typ_ut = typ_ut
        self.pohybl = pohybl
        self.power = power
        self.power_at_full_xp = power_at_full_xp
        self.druh = druh
        self.celkem_zivotu = celkem_zivotu
        self.podle = podle
        self.zkusenost = zkusenost

        self.nebezpecnost = 1.0
        """
        Coefficient for 'being targeted'
        """

        self.pocet_pred_utokem = None  # type: Optional[int]
        self.is_support = False

        self.temporary_for_round = False
        """
        Temporary units like Decoys that wanish at the end of round, dead or alive.
        """

    def can_attack_in_round(self, round_number: int) -> bool:
        if self.power <= 0:
            return False

        if self.is_shooter() or self.pohybl > 2:
            return True

        # yeah, could have been written like self.pohybl + round_number > 3.. verbose, but more readable
        if self.pohybl > 1 and round_number > 0:
            return True

        return round_number > 1

    def is_shooter(self):
        return self.typ_ut == 'S'

    def is_flier(self):
        return self.druh == 'L'

    def can_attack_unit(self, unit: 'Unit') -> bool:
        # only enemies
        if self.is_attacker_player == unit.is_attacker_player:
            return False

        # alive enemies
        if unit.pocet <= 0:
            return False

        # shooters attack wherever they want
        if self.is_shooter() or self.is_flier():
            return True

        # PB can attack only non-fliers
        return not unit.is_flier()

    def target_selection_rank(self, target_type: str, use_nebezpecnost: bool = True):
        base = 1.0
        if use_nebezpecnost:
            base = self.nebezpecnost

        if target_type == UnitTargeting.COUNT:
            return base * self.pocet

        if target_type == UnitTargeting.DAMAGE:
            return base * self.pocet * self.dmg

        if target_type == UnitTargeting.LIFE:
            return base * self.pocet * self.zivoty

        if target_type == UnitTargeting.INITIATIVE:
            # note that _z is not used here, Smich affects this, Ruzenec does not.
            return base * self.pocet * self.iniciativa

        raise Exception('Unknown targeting type %s' % target_type)

    def __str__(self) -> str:
        return '%d x %s' % (self.pocet, self.nazev)

    def str_in_combat_row(self):
        if self.pocet_pred_utokem == 1:
            return self.nazev

        return '%d x %s' % (self.pocet_pred_utokem, self.nazev)

    def attack_sign(self) -> str:
        return '=>' if self.is_attacker_player else '->'

    def printable_type(self) -> str:
        kind = 'L' if self.is_flier() else 'P'

        if self.is_shooter():
            return kind + 'S'

        if self.pohybl > 1:
            return '%sB%d' % (kind, self.pohybl)

        return kind + 'B'


class SpellInCombat:
    def __init__(self,
                 spell_id: int,
                 name: str,
                 text_template: str,
                 xp: float,
                 targeting: str,
                 counterspell: bool = False,
                 ):
        self.spell_id = spell_id
        self.name = name
        self.text_template = text_template
        self.xp = xp
        self.targeting = targeting
        self.counterspell = counterspell


class Config:

    def __init__(self) -> None:
        self.boj_base_succ = 0.9
        self.boj_base_random = 20
        self.boj_base_brneni = 0.3
        self.boj_base_brneni_random = 20
