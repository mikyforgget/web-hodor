# Create your views here.
import json
import traceback

import jsonpickle
from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from hodor.combat.battle import Battle
from hodor.combat.combatmodel import SpellInCombat
from hodor.combat.simulator import simulate_all
from hodor.combat.userinput import process_user_input, parse_input_file_autodetect, new_gate_units, parse_json
from hodor.data.provider import all_regular_units, combat_spell, COUNTERSPELL_ID, all_combat_spell_groups
from hodor.json import UnitFullInfoEncoder
from hodor.models import Tournament, TournamentResult, TournamentSubmission, UserSimulation
from hodor.tournament import execute_tournament, battle_one_on_one


def simul(request):
    if not settings.WEB_HODOR_ALLOW_PRECISE_SIMULATION:
        return render(request, 'hodor/simul/simul_disabled.html')

    # simulate heavily pre-scripted combat

    battles = []
    units_input = None
    gate_number = None
    input_gate_passed = None

    if request.method == 'POST':
        units_input = request.POST['units']
        gate_number = int(request.POST['gate_number'])
        input_gate_passed = int(request.POST['input_gate_passed']) if request.POST['input_gate_passed'] != '' else None

        gate_units_sets = new_gate_units(gate_number=gate_number, players_passed=input_gate_passed)

        for index, gate_units in enumerate(gate_units_sets):
            user_units = process_user_input(parse_input_file_autodetect(units_input))
            battle = Battle(user_units + gate_units, gate_number=gate_number, label='Army %d' % index,
                            verbose_result=True)
            battle.execute_battle()
            battles.append(battle)

        units_input = "\n".join('%s;%s;%d' % (u.nazev, ("%.4f" % (u.zkusenost / 100.0)).rstrip("0."), u.pocet) for u in
                                process_user_input(parse_input_file_autodetect(units_input)))

    return render(request, 'hodor/simul/simul.html', {
        'battles': battles,
        'units_input': units_input,
        'gate_number': gate_number,
        'input_gate_passed': input_gate_passed,
    })


@require_POST
@csrf_exempt  # no need for csrf here, on the other hand GET does not make sense
def ajax_simulate_gate(request):
    data = json.loads(request.body)

    attacker_units = process_user_input(parse_json(data['units']))
    battles = simulate_all(attacker_units, data['gate'], data['already_passed'])

    result = []

    for battle in battles:
        if battle.result_attacker + 0.05 < battle.result_defender:
            # solid victory
            result.append([battle.label, 'win'])
        elif battle.result_attacker < battle.result_defender:
            # close victory
            result.append([battle.label, 'near-win'])
        elif battle.result_attacker - 0.05 < battle.result_defender:
            # close defeat
            result.append([battle.label, 'near-loss'])
        else:
            # solid defeat
            result.append([battle.label, 'loss'])

    return JsonResponse({'result': result})


@cache_page(60 * 60)
def ajax_units(request):
    return JsonResponse({'units': all_regular_units()}, UnitFullInfoEncoder)


@permission_required('hodor.add_tournamentsubmission', raise_exception=True)
def tournament_admin(request, tournament_id: int, tournament_slug: str):
    tournament = get_object_or_404(Tournament, id=tournament_id)
    if tournament.slug != tournament_slug:
        redirect(reverse('hodor:tournament_admin', kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug}))

    tournament_results = {}
    for result in tournament.tournamentresult_set.all():
        if result.attacker_id not in tournament_results:
            tournament_results[result.attacker_id] = {}

        tournament_results[result.attacker_id][result.defender_id] = result

    return render(request, 'hodor/tournament/admin.html', {
        'tournament': tournament,
        'tournament_submissions': tournament.tournamentsubmission_set.order_by(*(['valid'] + tournament.result_type_submission_order_by())).all(),
        'tournament_results': tournament_results,
    })


def tournament_home(request):
    tournaments = Tournament.objects.order_by('-tournament_date').all()
    return render(request, 'hodor/tournament/home.html',
                  {
                      'tournaments': tournaments,
                  })


def tournament_builder(request):
    return render(request, 'hodor/tournament/builder.html')


def tournament_simul(request):
    battles = None
    error = None

    if request.method == 'POST':
        battles = []
        for left, right in (('attacker', 'defender'), ('defender', 'attacker')):

            attacker_army = request.POST['%s_army' % left]
            defender_army = request.POST['%s_army' % right]
            attacker_army_support_xp = request.POST.get('%s_army_support_xp' % left)
            defender_army_support_xp = request.POST.get('%s_army_support_xp' % right)
            attacker_army_support_xp = int(attacker_army_support_xp) if attacker_army_support_xp else 0
            defender_army_support_xp = int(defender_army_support_xp) if defender_army_support_xp else 0

            spells = {
                'attacker': [None, None, None, None],
                'defender': [None, None, None, None]
            }
            for attacker in ('attacker', 'defender'):
                for i in range(4):
                    spell_id = int(request.POST.get('spell_select_%s_%d' % (attacker, i), 0))
                    if spell_id:
                        spell = combat_spell(spell_id)

                        if i < spell.min_tu or i > spell.max_tu:
                            raise Exception('Cannot cast %s in %d' % (spell, i))

                        target = request.POST.get('spell_target_%s_%d' % (attacker, i))
                        spell_xp = int(request.POST.get('spell_xp_%s_%d' % (attacker, i))) / 100.0
                        if 0.01 > spell_xp > 1.0:
                            raise Exception("Spell XP not in range 1..100, got %d" % (spell_xp * 100))

                        # noinspection PyTypeChecker
                        spells[attacker][i] = SpellInCombat(
                            spell.spell_id, spell.name, spell.text_template, spell_xp, target, spell.spell_id == COUNTERSPELL_ID
                        )

            try:
                battle = battle_one_on_one(attacker_army, defender_army,
                                           attacker_army_support_xp / 100.0,
                                           defender_army_support_xp / 100.0,
                                           spells[left],
                                           spells[right],
                                           bool(request.POST.get('use_size_bonus')),
                                           )
                UserSimulation(army_attacker=attacker_army, army_defender=defender_army).save()

                battles.append(battle)
            except Exception as e:
                traceback.print_exc()
                error = str(e)

    combat_spells_by_groups = all_combat_spell_groups()

    return render(request, 'hodor/tournament/simul.html', {
        'battles': battles,
        'error': error,
        'combat_spells_by_groups': combat_spells_by_groups,
    })


def tournament_detail(request, tournament_id: int, tournament_slug: str):
    tournament = get_object_or_404(Tournament, id=tournament_id)
    if tournament.slug != tournament_slug:
        redirect(reverse('hodor:tournament_detail', kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug}))

    return render(request, 'hodor/tournament/detail.html', {
        'tournament': tournament,
    })


def tournament_result(request, tournament_id: int, tournament_slug: str):
    tournament = get_object_or_404(Tournament, id=tournament_id)
    if tournament.slug != tournament_slug:
        redirect(reverse('hodor:tournament_result', kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug}))

    if tournament.publicly_visible:
        tournament_results = {}
        for result in tournament.tournamentresult_set.all():
            if result.attacker_id not in tournament_results:
                tournament_results[result.attacker_id] = {}

            tournament_results[result.attacker_id][result.defender_id] = result

        return render(request, 'hodor/tournament/result.html', {
            'tournament': tournament,
            'tournament_submissions': tournament.tournamentsubmission_set.order_by(*tournament.result_type_submission_order_by()).all(),
            'tournament_results': tournament_results,
        })

    else:
        return render(request, 'hodor/tournament/result_unfinished.html', {
            'tournament': tournament,
            'tournament_submissions': tournament.tournamentsubmission_set.order_by(*tournament.result_type_submission_order_by())[:3],
        })


def tournament_result_player(request, tournament_id: int, tournament_slug: str, player_id: int):
    tournament = _get_publicly_visible_tournament(request, tournament_id)
    if tournament.slug != tournament_slug:
        redirect(reverse('hodor:tournament_result_player', kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug, 'player_id': player_id}))

    tournament_submission = get_object_or_404(TournamentSubmission, pk=player_id)

    results = TournamentResult.objects.select_related('defender').filter(tournament=tournament).filter(Q(attacker__id=player_id) | Q(defender__id=player_id)).all()

    result_matrix = {}

    for result in results:
        if result.attacker_id == player_id:
            opponent_id = result.defender.player_id
            opponent_name = result.defender.player_name
            opponent_submission_id = result.defender.id
            attack = True
        else:
            opponent_id = result.attacker.player_id
            opponent_name = result.attacker.player_name
            opponent_submission_id = result.attacker.id
            attack = False

        if opponent_id not in result_matrix:
            result_matrix[opponent_id] = {
                'opponent_id': opponent_id,
                'opponent_name': opponent_name,
                'opponent_submission_id': opponent_submission_id,
            }

        if attack:
            result_matrix[opponent_id]['attack'] = result
        else:
            result_matrix[opponent_id]['defense'] = result

    result_list = list(result_matrix.values())
    result_list.sort(key=lambda r: -(r['attack'].defender_losses + r['defense'].attacker_losses - r['attack'].attacker_losses - r['defense'].defender_losses))

    return render(request, 'hodor/tournament/result_player.html', {
        'tournament_submission': tournament_submission,
        'tournament': tournament,
        'result_list': result_list,
    })


def tournament_result_match(request, tournament_id: int, tournament_slug: str, attacker_id: int, defender_id: int):
    tournament = _get_publicly_visible_tournament(request, tournament_id)
    if tournament.slug != tournament_slug:
        redirect(reverse('hodor:tournament_result_match',
                         kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug, 'attacker_id': attacker_id, 'defender_id': defender_id}))

    tournament_result = get_object_or_404(TournamentResult,
                                          tournament=tournament,
                                          attacker__id=attacker_id,
                                          defender__id=defender_id)

    # hodor.combat.logevent
    # backwards compatible hack
    json = tournament_result.log_events_json.replace('"hodor.combat.combatmodel.', '"hodor.combat.logevent.')

    return render(request, 'hodor/tournament/result_match.html', {
        'tournament': tournament,
        'tournament_result': tournament_result,
        'log_events': jsonpickle.decode(json),
    })


@permission_required('hodor.add_tournamentsubmission', raise_exception=True)
@require_POST
def tournament_execute(request, tournament_id: int, tournament_slug: str):
    tournament = get_object_or_404(Tournament, id=tournament_id)

    execute_tournament(tournament)

    return redirect(reverse('hodor:tournament_admin', kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug}))


@permission_required('hodor.add_tournamentsubmission', raise_exception=True)
@require_POST
def tournament_validate(request, tournament_id: int, tournament_slug: str):
    tournament = get_object_or_404(Tournament, id=tournament_id)

    execute_tournament(tournament, validate_only=True)

    return redirect(reverse('hodor:tournament_admin', kwargs={'tournament_id': tournament.id, 'tournament_slug': tournament.slug}))


def info_units(request):
    return render(request, 'hodor/info/units.html', {
        'units_info': all_regular_units()
    })


def info_sizes(request):
    return render(request, 'hodor/info/sizes.html')


def _get_publicly_visible_tournament(request, tournament_id: int) -> Tournament:
    if request.user.is_superuser:
        return get_object_or_404(Tournament, id=tournament_id)

    return get_object_or_404(Tournament, id=tournament_id, publicly_visible=True)
