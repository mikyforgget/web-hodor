#!/bin/bash

echo $(date '+%Y-%m-%d %H:%M:%S') Redeploying...

# stop app
supervisorctl stop app

# load env
source /srv/sgmmelioranniscom.env.sh
echo $(date '+%Y-%m-%d %H:%M:%S') App stopped...

cd /srv/app/web-hodor
git pull

# migrate
python manage.py migrate
echo $(date '+%Y-%m-%d %H:%M:%S') Migrated

# static
python manage.py collectstatic --noinput -v0
echo $(date '+%Y-%m-%d %H:%M:%S') Static resources collected

# start app
supervisorctl start app
echo $(date '+%Y-%m-%d %H:%M:%S')  App Started
