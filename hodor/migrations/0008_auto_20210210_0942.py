# Generated by Django 3.1.2 on 2021-02-10 08:42

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('hodor', '0007_budovy_jednotky_kouzla'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tournament',
            old_name='intro',
            new_name='intro_markdown',
        ),
    ]
