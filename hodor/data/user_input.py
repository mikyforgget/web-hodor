class UserInput:

    def __init__(self,
                 name: str = None,
                 experience: float = 0.4,
                 count: int = None,
                 power: int = None,
                 unit_id: int = None,
                 targeting: str = None,
                 is_support: bool = False,
                 ):
        self.name = name
        self.unit_id = unit_id
        self.experience = experience
        self.count = count
        self.power = power
        self.targeting = targeting
        self.is_support = is_support
