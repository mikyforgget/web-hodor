import random
from typing import Iterable, List, Optional

from hodor.combat.combatmodel import Unit, SpellInCombat, Config
from hodor.combat.func import battle_area_gain
from hodor.combat.logevent import LogEvent, GateAttacked, CombatResult, CombatVerboseResult, SupportJoined, RoundStarted, AttackNoTarget, Attack, AreaTaken
from hodor.combat.sizerank import size_bonus
from hodor.combat.spells import SpellHelper


class Battle:
    def __init__(self,
                 units: Iterable[Unit],
                 gate=True,
                 verbose_result=False,
                 quiet=False,
                 debug_enabled=False,
                 gate_number: int = None,
                 label: str = None,
                 attacker_spells: List[SpellInCombat] = (None, None, None, None),
                 defender_spells: List[SpellInCombat] = (None, None, None, None),
                 ) -> None:
        self.units = units
        self.gate = gate

        self.config = Config()

        self.log_events = []  # type: List[LogEvent]

        self.round = 0
        """zero indexed"""

        self.suppress_random = True
        self.debug_enabled = debug_enabled
        # there is a bug that 'druh' is renamed to 'druh_jednotky' and thus does not work correctly
        self.simulate_bug_druh_druh_jednotku = True
        self.verbose_result = verbose_result
        self.quiet = quiet
        """overrides verbose and debug, only returns combat result, saves a bit on memory"""

        self.gate_number = gate_number

        self.result_attacker = None
        self.result_defender = None
        self.label = label

        self.attacker_spells = attacker_spells
        self.defender_spells = defender_spells
        self.attacker_spell_xp_boost = 0.0
        """
        Constant gain to spell xp (0.1 = 10%). Does not work on 7001 and 5505
        """
        self.defender_spell_xp_boost = 0.0
        """
        Constant gain to spell xp (0.1 = 10%). Does not work on 7001 and 5505
        """
        self.spell_helper = SpellHelper(self)

        self.log_area_taken = False
        self.area_taken = None

        self.use_size_bonus = False
        """
        Use small < medium < large < small bonuses.
        """

        if gate:
            self.log_event(GateAttacked(self.gate_number))

    def log_event(self, event: LogEvent):
        if not self.quiet:
            self.log_events.append(event)

    def debug(self, text):
        if self.debug_enabled and not self.quiet:
            print('DEBUG: %s' % text)

    def print_battle(self):
        for log_event in self.log_events:
            print(log_event.as_ansi())
            print()

    def execute_battle(self):
        self.log_support_units()

        for i in range(4):
            self.execute_round()

        self.result()

    def result(self):
        attacked_down = 0.0
        attacker_total = 0.0
        defender_down = 0.0
        defender_total = 0.0

        for unit in self.units:
            if unit.is_attacker_player:
                attacked_down += (unit.pocet_puvodni - unit.pocet) * unit.power
                attacker_total += unit.pocet_puvodni * unit.power
            else:
                defender_down += (unit.pocet_puvodni - unit.pocet) * unit.power
                defender_total += unit.pocet_puvodni * unit.power

        self.debug('attacked_down %.2f attacker_total %.2f' % (attacked_down, attacker_total))
        self.debug('defender_down %.2f defender_total %.2f' % (defender_down, defender_total))

        self.result_attacker = attacked_down / attacker_total
        self.result_defender = defender_down / defender_total

        self.log_event(CombatResult(self.result_attacker, self.result_defender))

        if self.log_area_taken:
            self.area_taken = battle_area_gain(self.result_attacker, self.result_defender)
            if self.area_taken > 0.0:
                self.log_event(AreaTaken(self.area_taken))

        if self.verbose_result:
            self.log_event(CombatVerboseResult(
                attacker_power_remaining=int(attacker_total - attacked_down),
                attacker_power_total=int(attacker_total),
                defender_power_remaining=int(defender_total - defender_down),
                defender_power_total=int(defender_total),
                attacker_units=(unit for unit in sorted(self.units, key=lambda u: -u.pocet_puvodni * u.power) if unit.is_attacker_player),
                defender_units=(unit for unit in sorted(self.units, key=lambda u: -u.pocet_puvodni * u.power) if not unit.is_attacker_player),
            ))

    def log_support_units(self):
        attacker_support = []
        defender_support = []
        for unit in self.units:
            if unit.is_support and unit.is_attacker_player:
                attacker_support.append(unit)
            if unit.is_support and not unit.is_attacker_player:
                defender_support.append(unit)

        if attacker_support or defender_support:
            self.log_event(SupportJoined(attacker_support, defender_support))

    def execute_round(self):
        self.log_event(RoundStarted(self.round))

        self.spell_helper.cast_spells_for_round()

        for attacker in self.attackers_for_round():
            if attacker.pocet <= 0:
                continue

            self.perform_attack(attacker)

        self.round_cleanup()
        self.round += 1

    def round_cleanup(self):
        for unit in self.units:
            unit.protiutok_poc = unit.protiutok_poc_puvodni
            unit.iniciativa_z = unit.iniciativa
            unit.dmg_z = unit.dmg

        # remove temps
        self.units = list(unit for unit in self.units if not unit.temporary_for_round)

    def attackers_for_round(self):
        return sorted(
            (unit for unit in self.units if unit.can_attack_in_round(self.round)),
            key=lambda unit: unit.iniciativa_z,
            reverse=True
        )

    def perform_attack(self, attacker: Unit):
        defender = self.select_target(attacker)
        attacker.pocet_pred_utokem = attacker.pocet

        if defender is None:
            self.log_event(AttackNoTarget(attacker))
            return

        defender.pocet_pred_utokem = defender.pocet

        self.calculate_attack(attacker, defender, 1.0)

        if defender.protiutok_poc > 0:
            counterstrike_coefficient = self.counterstrike_coefficient(attacker, defender)

            if counterstrike_coefficient > 0:
                defender.protiutok_poc -= 1

                self.calculate_attack(defender, attacker, counterstrike_coefficient, True)

                attacker.pocet_pred_utokem = attacker.pocet

        defender.pocet_pred_utokem = defender.pocet

    @staticmethod
    def counterstrike_coefficient(attacker: Unit, defender: Unit) -> float:
        # sub protiutok_ano_ne {}

        if attacker.is_shooter():
            return 0

        coefficient = 1.0
        if defender.is_shooter():
            coefficient /= 4
        else:
            coefficient /= 2

        if attacker.is_flier():
            coefficient /= 2
        else:
            coefficient /= 1.5

        return coefficient

    def select_target(self, attacker: Unit) -> Optional[Unit]:
        targets = list(
            unit
            for unit
            in self.units
            if attacker.can_attack_unit(unit)
        )

        targets = sorted(
            targets,
            key=lambda unit: unit.target_selection_rank(attacker.podle),
            reverse=True
        )

        if not targets:
            return None

        return targets[0]

    def rand(self, min_value: float, max_value: float) -> float:
        if self.suppress_random:
            return (min_value + max_value) / 2

        return random.uniform(min_value, max_value)

    def rand_base_random(self, min_value: float, random_hundreds: int) -> float:
        return self.rand(min_value, min_value + random_hundreds / 100)

    def round_coefficient(self):
        if self.gate:
            return 1.0

        return (0.6, 0.8, 1, 0.7)[self.round]

    def calculate_attack(self, attacker: Unit, defender: Unit, damage_coefficient: float, is_counterattack=False):
        # sub vypocitat_utok {}

        assert attacker.pocet_pred_utokem > 0
        assert defender.pocet_pred_utokem > 0

        # possibly not required? but still, copy paste from src
        if attacker.zivoty <= 0:
            attacker.zivoty = 0.1
        if attacker.zkusenost < 1:
            attacker.zkusenost = 1
        if defender.zivoty <= 0:
            defender.zivoty = 0.1
        if defender.brneni_celkem_puvodni < 0:
            defender.brneni_celkem_puvodni = 0

        # TODO take from config?
        # my $uspesnost_dmg = $BOJ_BASE_SUCC + (rand($BOJ_BASE_RANDOM) / 100);
        damage_success = self.rand_base_random(self.config.boj_base_succ, self.config.boj_base_random)
        damage = int(
            attacker.dmg_z * attacker.pocet_pred_utokem * attacker.zkusenost / 100 * damage_success * damage_coefficient * self.round_coefficient() + 0.5
        )

        self.debug('%s attacks %s, damage_success %.2f, damage %d' % (
            attacker, defender, damage_success, damage
        ))
        damage_coefficient_based_on_units = self.damage_coefficient_based_on_units(attacker, defender)
        damage = int(damage * damage_coefficient_based_on_units)

        damage_coefficient_based_on_size = self.damage_coefficient_based_on_size(attacker, defender)
        damage = int(damage * damage_coefficient_based_on_size)

        self.debug('damage after reduction of %s vs %s = %d (%.2f%%)' % (
            attacker.printable_type(), defender.printable_type(), damage, damage_coefficient_based_on_units * 100.0
        ))

        # my $relativni_dmg_max = $dmg * 15;
        relative_damage_max = damage * 15
        armor_remaining = 0

        if defender.brneni_celkem > 0:
            # my $koeficient_brneni_pouzit = ($BOJ_BASE_BRNENI + (rand($BOJ_BASE_BRNENI_RANDOM) / 100));
            coefficient_armor_used = self.rand_base_random(self.config.boj_base_brneni,
                                                           self.config.boj_base_brneni_random)
            armor_used = int(defender.brneni_celkem * coefficient_armor_used)

            self.debug('coefficient_armor_used: %.2f, armor used %d' % (coefficient_armor_used, armor_used))

            if armor_used > damage * defender.zkusenost / 100:
                armor_used = int(damage * defender.zkusenost / 100)
                self.debug(
                    "armor_used reduced to %d based on defender experience %.2f%%" % (armor_used, defender.zkusenost))

            if armor_used > damage * coefficient_armor_used:
                armor_used = int(damage * coefficient_armor_used)
                self.debug("armor_used reduced to %d based on coefficient_armor_used %.2f" % (
                    armor_used, coefficient_armor_used))

            damage -= armor_used
            defender.brneni_celkem -= armor_used

            self.debug('damage reduced to %d, def.brneni celkem %d' % (damage, defender.brneni_celkem))

            if defender.brneni_celkem_puvodni > 0:
                armor_remaining = int(100 * defender.brneni_celkem / defender.brneni_celkem_puvodni)
            else:
                # TODO not really necessary, it will default in here
                armor_remaining = 0

        if damage > defender.celkem_zivotu:
            damage = defender.celkem_zivotu
            self.debug("damage reduced to %d as it's over defenders life" % damage)

        defender.celkem_zivotu -= damage

        self.debug('defender.celkem_zivotu after damage %.2f' % defender.celkem_zivotu)
        if not defender.zivoty:
            defender.celkem_zivotu = 0
            assert False, "Can this happen??"

        remaining = 0
        if defender.zivoty > 0.01:
            remaining = defender.celkem_zivotu / defender.zivoty

        # my $sejmout = $obrana->{'pocet_pred_utokem'} - $kolik_zbyva;
        to_kill = defender.pocet_pred_utokem - remaining
        if to_kill < 0:
            assert False, "Can this happen??"

        kills_max = to_kill

        self.debug('remaining %d, to_kill %d, kills_max %d' % (remaining, to_kill, kills_max))
        if defender.zivoty:
            # TODO int()?
            kills_max = relative_damage_max / defender.zivoty
            self.debug('reduced max to_kill to %d based on relative_damage_max = %d and defender.zivoty = %.2f' % (
                kills_max, relative_damage_max, defender.zivoty
            ))

        if to_kill > kills_max:
            to_kill = kills_max
            self.debug("reduced number of to_kill to %d because of kills_max being %d" % (to_kill, kills_max))

        if defender.celkem_zivotu:
            pseudo_life_total = defender.pocet_pred_utokem * defender.zivoty
            # my $zabiti = 1 - ($obrana->{'zivoty_celkem'} / $pseudozivoty_celkem / 2.2);
            kill_coefficient = 1 - (defender.celkem_zivotu / pseudo_life_total / 2.2)
            killed = int(kill_coefficient * to_kill)
        else:
            killed = defender.pocet_pred_utokem

        self.debug('to_kill (sejmout) %d, killed (zabito) %d' % (to_kill, killed))

        if killed and defender.brneni_celkem and defender.pocet_pred_utokem:
            coefficient_armor_reduction = 1 - killed / defender.pocet_pred_utokem
            defender.brneni_celkem = defender.brneni_celkem * coefficient_armor_reduction
            defender.brneni_celkem_puvodni = defender.brneni_celkem_puvodni * coefficient_armor_reduction

            self.debug('Reducing armor by %.2f coefficient, new brneni_celkem %.2f, new brneni_celkem_puvodni %.2f' % (
                coefficient_armor_reduction, defender.brneni_celkem, defender.brneni_celkem_puvodni
            ))

        wounded = int(to_kill - killed + 0.5) + 1
        self.debug('wounded %d' % wounded)

        if wounded + killed > defender.pocet_pred_utokem:
            defender.pocet -= killed
            if defender.pocet < wounded:
                wounded = defender.pocet
                self.debug(
                    "reduced wounded to %d as it's lower than current defender.pocet %d" % (wounded, defender.pocet))
        else:
            defender.pocet -= killed

        self.debug("defender.pocet reduced to %d (from defender.pocet_pred_utokem %d)" % (
            defender.pocet, defender.pocet_pred_utokem))

        if wounded or killed:
            # assigning XP! :)
            experience_ratio = (
                    (wounded + killed)
                    * defender.dmg_z * defender.zkusenost
                    / (attacker.pocet_pred_utokem * attacker.dmg_z * 100 + 1)
                    * (110 - attacker.zkusenost)
                    / 20)

            self.debug("attacker experience_ratio %.5f" % experience_ratio)
            if not attacker.is_shooter() and (self.simulate_bug_druh_druh_jednotku or not attacker.is_flier()):
                experience_ratio *= 3
                self.debug("increased attacker experience_ratio to %.3f for %s" % (
                    experience_ratio, attacker.printable_type()))

            if experience_ratio > 0.5:
                experience_ratio = 0.5
                self.debug("increased attacker experience_ratio to %.3f cap" % experience_ratio)

            experience_boost = experience_ratio * damage_success * 5
            attacker.zkusenost += experience_boost
            if attacker.zkusenost > 100:
                attacker.zkusenost = 100.0

            self.debug("increased experience of attacker %s by %.3f%% to %.3f%%" % (
                attacker, experience_boost, attacker.zkusenost))

            # assigning XP to defender
            experience_ratio = (
                    (wounded + killed)
                    * attacker.zivoty / attacker.zkusenost
                    / (defender.pocet_pred_utokem * defender.zivoty / 100 + 1)
                    * (110 - defender.zkusenost)
                    / 20)

            self.debug("defender experience_ratio %.3f" % experience_ratio)
            if attacker.is_shooter():
                experience_ratio *= 0.2
                self.debug("defender experience_ratio reduced by 0.2 to %.3f for shooter attacker" % experience_ratio)

            if experience_ratio > 50:
                experience_ratio = 50
                self.debug("defender experience_ratio capped to %2.f" % experience_ratio)

            experience_boost = experience_ratio / 100
            defender.zkusenost += experience_boost
            if defender.zkusenost > 100:
                defender.zkusenost = 100.0

            self.debug("increased experience of defender %s by %.3f%% to %.3f%%" % (
                defender, experience_boost, defender.zkusenost))

        wounded_total = 100
        if defender.pocet * defender.zivoty:
            wounded_total = int(100 * (1 - defender.celkem_zivotu / defender.zivoty / defender.pocet))

        self.debug("wounded_total %d" % wounded_total)
        if wounded_total > 100:
            wounded_total = 100
            self.debug("capping wounded_total at %d" % wounded_total)

        message = "%s %s %s" % (attacker.str_in_combat_row(), attacker.attack_sign(), defender.str_in_combat_row())
        message += "\nzabito: %d, zbývá: %d / %d" % (killed, defender.pocet, defender.pocet_puvodni)
        if defender.pocet:
            message += " zraněno: %d%%" % wounded_total
        if armor_remaining and defender.pocet:
            message += "\nbrnění zbývá: %d%%" % armor_remaining

        self.log_event(Attack(
            attacker, defender, killed, 0, wounded_total, armor_remaining, is_counterattack
        ))

        self.debug('--------------------------------')

    def damage_coefficient_based_on_units(self, attacker: Unit, defender: Unit) -> float:
        if self.simulate_bug_druh_druh_jednotku:
            return 1.0

        # sub snizeni_dmg_dle_typu_jednotek {}

        coefficient = 1.0

        if attacker.is_shooter():
            if not attacker.is_flier():

                if defender.is_flier():
                    # PS vs Lx
                    coefficient *= 1.2
                else:
                    # PS v Px
                    coefficient *= 0.7

            if not defender.is_flier() and defender.pohybl > 1:
                # shooters against phb2/phb3, bugged though, should have been 'is not shooter'
                coefficient *= 0.8

        if attacker.is_flier():
            # Lx vs xS
            if defender.is_shooter():
                coefficient *= 0.5

        return coefficient

    def damage_coefficient_based_on_size(self, attacker: Unit, defender: Unit) -> float:
        if not self.use_size_bonus:
            return 1.0

        return size_bonus(attacker.power_at_full_xp, defender.power_at_full_xp)
