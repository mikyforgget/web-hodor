from enum import Enum
from typing import Iterable

from hodor.combat.combatmodel import Unit
from hodor.combat.format import nft
from hodor.combat.func import AREA_20K_MUL


class AnsiColors(Enum):
    # see https://en.wikipedia.org/wiki/ANSI_escape_code
    YELLOW = '\033[33m'
    BRIGHT_YELLOW = '\033[93m'
    DIM_ITALICS = '\033[90m\033[3m'
    BOLD = '\033[1m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    RESET = '\033[0m'

    def __str__(self):
        return self.value


class LogEvent:

    def __init__(self, ansi_color: AnsiColors = None) -> None:
        self.ansi_color = ansi_color

    def as_ascii(self) -> str:
        pass

    def as_ansi(self) -> str:
        if self.ansi_color is not None:
            return '%s%s%s' % (self.ansi_color, self.as_ascii(), AnsiColors.RESET)
        return self.as_ascii()


class GateAttacked(LogEvent):
    def __init__(self, gate_number: int) -> None:
        super().__init__(AnsiColors.BOLD)
        self.gate_number = gate_number

    def as_ascii(self):
        return 'Můj pane, zaútočil jste na %d. bránu, jejímž obráncem je %s se svou družinou.' % (
            self.gate_number,
            ('Klotho', 'Lachesis', 'Atropos', 'Caerme', 'Argo', 'Furia', 'Celesta', 'Tara')[self.gate_number - 1],
        )


class CombatResult(LogEvent):
    def __init__(self, attacker_losses: float, defender_losses: float) -> None:
        super().__init__(AnsiColors.BOLD)
        self.attacker_losses = attacker_losses
        self.defender_losses = defender_losses

    def as_ascii(self):
        return 'Vaše ztráty jsou %.2f%% síly armády, obránce %.2f%% síly armády' % (
            self.attacker_losses * 100.0, self.defender_losses * 100.0)


class RoundStarted(LogEvent):
    def __init__(self, round_number: int) -> None:
        super().__init__(AnsiColors.BOLD)
        self.round = round_number

    def as_ascii(self):
        return "%d. kolo" % (self.round + 1)


class SupportJoined(LogEvent):
    def __init__(self,
                 attacker_support: Iterable[Unit],
                 defender_support: Iterable[Unit]) -> None:
        super().__init__(AnsiColors.BOLD)
        self.attacker_support = list(LogUnit(unit) for unit in attacker_support)
        self.defender_support = list(LogUnit(unit) for unit in defender_support)

    def as_ascii(self):
        messages = []
        for unit in self.attacker_support:
            messages.append('K armádě útočníka se připojilo %d x %s' % (unit.count, unit.name))
        for unit in self.defender_support:
            messages.append('K armádě obránce se připojilo %d x %s' % (unit.count, unit.name))

        return "\n".join(messages)


class AreaTaken(LogEvent):
    def __init__(self, area_taken: float) -> None:
        super().__init__(AnsiColors.BOLD)
        self.area_taken = area_taken

    def as_ascii(self):
        return 'Útočník získává %d hAR.' % int(self.area_taken * AREA_20K_MUL)


class CombatVerboseResult(LogEvent):
    def __init__(self,
                 attacker_power_total: int,
                 attacker_power_remaining: int,
                 defender_power_total: int,
                 defender_power_remaining: int,
                 attacker_units: Iterable[Unit],
                 defender_units: Iterable[Unit]) -> None:
        super().__init__()

        self.attacker_power_remaining = attacker_power_remaining
        self.attacker_power_total = attacker_power_total
        self.defender_power_remaining = defender_power_remaining
        self.defender_power_total = defender_power_total
        self.attacker_units = list(LogUnit(unit) for unit in attacker_units)
        self.defender_units = list(LogUnit(unit) for unit in defender_units)

    def as_ascii(self):
        message = self.as_ansi()

        for ansi_color in AnsiColors:
            message = message.replace(ansi_color.value, '')
        return message

    def as_ansi(self):
        message = ("%sCelková síla útočník: %s / %s%s\nCelková síla obránce: %s / %s\n\n" % (
            AnsiColors.YELLOW,
            nft(self.attacker_power_remaining), nft(self.attacker_power_total),
            AnsiColors.RESET,
            nft(self.defender_power_remaining), nft(self.defender_power_total)
        ))

        messages = [
            'Jednotka                   typ       počet       zbývá        síla   zb. síla   přežilo  celk.ztrát',
            '-------------------------  ---  ----------  ----------  ----------  ----------  -------  ----------',
        ]
        for unit in self.defender_units + self.attacker_units:
            down = (unit.count_initial - unit.count) * unit.power
            total = self.attacker_power_total if unit.is_attacker_player else self.defender_power_total

            row = "%-25s  %-3s  %10s  %10s  %10s  %10s  %6.2f%%     %6.2f%%" % (
                unit.name,
                unit.printable_type,
                nft(unit.count_initial),
                nft(unit.count),
                nft(int(unit.count_initial * unit.power)),
                nft(int(unit.count * unit.power)),
                (unit.count / unit.count_initial) * 100.0,
                down / total * 100.0
            )

            if unit.is_attacker_player:
                row = '%s%s%s' % (AnsiColors.YELLOW, row, AnsiColors.RESET)

            messages.append(row)

        message += "\n".join(messages)

        return message


class CastSpell(LogEvent):
    def __init__(self,
                 is_attacker: bool,
                 success: bool,
                 target_found: bool = True,
                 spell_name: str = True,
                 spell_text: str = None) -> None:
        super().__init__()

        self.is_attacker = is_attacker
        self.success = success
        self.target_found = target_found
        self.spell_name = spell_name
        self.spell_text = spell_text
        if self.spell_text is not None:
            for br in ('<br>', '<br/>', '<br />'):
                self.spell_text = self.spell_text.replace(br, "\n")

            self.spell_text = self.spell_text.replace('xxNEPRxx', 'nepřítel')
            self.spell_text = self.spell_text.replace('xxSESILxx', 'sesilatel')

    def as_ascii(self):
        if self.is_attacker:
            message = 'Útočník '
        else:
            message = 'Obránce '

        message += 'sesílá "%s"\n' % self.spell_name
        if not self.target_found:
            message += '%s%s' % (AnsiColors.RED, 'Kouzlo nenašlo svůj cíl.')
        elif self.success:
            message += '%s%s' % (AnsiColors.GREEN, self.spell_text)
        else:
            message += '%s%s' % (AnsiColors.RED, 'Seslání nebylo dokončeno úspěšně, protivník sesílané kouzlo magicky zastavil.')

        return message


class LogUnit:

    def __init__(self, unit: Unit) -> None:
        self.name = unit.nazev
        self.count = unit.pocet
        self.count_before_attack = unit.pocet_pred_utokem
        self.count_initial = unit.pocet_puvodni
        self.is_attacker_player = unit.is_attacker_player
        self.power = unit.power
        self.printable_type = unit.printable_type()

        self.rich = True
        self.initiative = unit.iniciativa
        self.initiative_z = unit.iniciativa_z
        self.xp = unit.zkusenost / 100.0
        self.damage = unit.dmg_z
        self.armor_total = unit.brneni_celkem
        self.armor_total_initial = unit.brneni_celkem_puvodni
        self.life = unit.zivoty
        self.life_total = unit.celkem_zivotu
        self.target = unit.podle
        self.type = unit.printable_type()

    def attack_sign(self) -> str:
        return '=>' if self.is_attacker_player else '->'

    def __str__(self) -> str:
        if self.count_before_attack > 1:
            return '%d x %s' % (self.count_before_attack, self.name)

        return self.name


class AttackNoTarget(LogEvent):

    def __init__(self, attacker: Unit) -> None:
        super().__init__()
        self.unit = LogUnit(attacker)
        if self.unit.is_attacker_player:
            self.ansi_color = AnsiColors.YELLOW

    def as_ascii(self):
        return "%s %s Jednotka nemá žádný cíl" % (self.unit, self.unit.attack_sign())


class Attack(LogEvent):
    def __init__(self,
                 attacker: Unit,
                 defender: Unit,
                 killed: int,
                 remaining: int,
                 wounded: int,
                 armor_remaining: int,
                 counter_attack: bool) -> None:
        super().__init__()
        self.attacker = LogUnit(attacker)
        self.defender = LogUnit(defender)
        self.killed = killed
        self.remaining = remaining
        self.wounded = wounded
        self.armor_remaining = armor_remaining
        self.counter_attack = counter_attack

        if self.counter_attack:
            self.ansi_color = AnsiColors.DIM_ITALICS
        elif self.attacker.is_attacker_player:
            self.ansi_color = AnsiColors.YELLOW

    def as_ascii(self):
        message = "%s %s %s" % (self.attacker, self.attacker.attack_sign(), self.defender)
        message += "\nzabito: %d, zbývá: %d / %d" % (
            self.killed, self.defender.count, self.defender.count_initial)
        if self.defender.count > 0:
            message += " zraněno: %d%%" % self.wounded
            if self.armor_remaining:
                message += "\nbrnění zbývá: %d%%" % self.armor_remaining

        if self.counter_attack:
            message = "obrana:\n" + message

        return message
