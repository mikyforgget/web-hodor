import math
import time
from typing import Iterable, List, Optional

import jsonpickle

from hodor.combat.battle import Battle
from hodor.combat.combatmodel import UserInput, Unit, UnitMetaType, UnitTargeting, SpellInCombat
from hodor.combat.userinput import single_unit
from hodor.data.provider import static_unit_by_id, static_unit_by_name, unit_full_info_by_id
from hodor.models import Tournament, TournamentResult, TournamentSubmission


def execute_tournament(tournament: Tournament, validate_only: bool = False):
    submissions = tournament.tournamentsubmission_set.all()

    valid_submissions = []

    for submission in submissions:
        try:
            units = process_csv(parse_csv(submission.army), is_attacker=True)
            validate_submission(tournament, submission, units)
            submission.valid = True
            submission.validation_message = None
            valid_submissions.append(submission)
        except Exception as e:
            submission.valid = False
            submission.validation_message = str(e)
            submission.save()

    if validate_only:
        # proc delayed save
        for submission in valid_submissions:
            submission.save()

        return

    TournamentResult.objects.filter(tournament=tournament).delete()

    submission_stats = {}
    for submission in valid_submissions:
        submission_stats[submission.id] = {
            'wins': 0,
            'battles': 0,
            'sum_result_diff': 0.0,
            'submission': submission,
            'area_taken': 0.0,
        }

    start_time = time.time()
    battles_count = 0
    for attacker in valid_submissions:
        for defender in valid_submissions:
            if attacker == defender:
                continue

            battles_count += 1

            battle = battle_one_on_one(attacker.army, defender.army,
                                       tournament.support_xp, tournament.support_xp,
                                       use_size_bonus=tournament.use_size_bonus)

            for log_event in battle.log_events:
                log_event.ansi_color = None

            diff = battle.result_defender - battle.result_attacker
            if diff > 0.02:  # win by more than 2%
                submission_stats[attacker.id]['wins'] += 1
            elif diff < -0.02:  # loss by more than 2%
                submission_stats[defender.id]['wins'] += 1

            submission_stats[attacker.id]['battles'] += 1
            submission_stats[attacker.id]['sum_result_diff'] += diff
            submission_stats[attacker.id]['area_taken'] += battle.area_taken
            submission_stats[defender.id]['battles'] += 1
            # mind the negative, defender counts opposite casualties
            submission_stats[defender.id]['sum_result_diff'] += -diff
            submission_stats[defender.id]['area_taken'] -= battle.area_taken

            TournamentResult(
                tournament=tournament,
                attacker=attacker,
                defender=defender,
                attacker_losses=battle.result_attacker,
                defender_losses=battle.result_defender,
                log_events_json=jsonpickle.encode(battle.log_events),  # note the erasure of color above
            ).save()
    print("Executed %d battles in %.2f seconds" % (battles_count, time.time() - start_time))

    for stats in submission_stats.values():
        submission = stats['submission']
        submission.wins = stats['wins']
        submission.average_result_diff = stats['sum_result_diff'] / stats['battles']
        submission.area_taken = stats['area_taken']

    for submission in submissions:
        submission.save()


def battle_one_on_one(attacker_army_str: str,
                      defender_army_str: str,
                      attacker_army_support_xp: Optional[float] = None,
                      defender_army_support_xp: Optional[float] = None,
                      attacker_spells: List[Optional[SpellInCombat]] = (None, None, None, None),
                      defender_spells: List[Optional[SpellInCombat]] = (None, None, None, None),
                      use_size_bonus: bool = False,
                      ) -> Battle:
    attacker_units = process_csv(parse_csv(attacker_army_str), is_attacker=True)
    attacker_units += _support_units(attacker_units, True, attacker_army_support_xp)
    defender_units = process_csv(parse_csv(defender_army_str), is_attacker=False)
    defender_units += _support_units(defender_units, False, defender_army_support_xp)

    units = attacker_units + defender_units

    battle = Battle(units=units, gate=False, verbose_result=True, attacker_spells=attacker_spells, defender_spells=defender_spells, )
    battle.log_area_taken = True
    battle.use_size_bonus = use_size_bonus
    battle.execute_battle()

    return battle


def _support_units(army: List[Unit], is_attacker: bool, support_xp: Optional[float]) -> List[Unit]:
    if support_xp is None or support_xp == 0:
        return []

    if support_xp < 0 or support_xp > 1:
        raise Exception('Incorrect XP for support, 0..1.0 required, got %4.f' % support_xp)

    total_power = sum(unit.power * unit.pocet for unit in army)
    support_stack_power = total_power * 0.2 / 2

    result = []
    for unit_id in (5518, 6018):
        static_unit = static_unit_by_id(unit_id)
        count = int(support_stack_power / static_unit.power)
        if count > 0:
            unit = single_unit(static_unit, support_xp, count, is_attacker, 1.1, targeting_override=UnitTargeting.COUNT)
            unit.is_support = True
            result.append(unit)

    return result


def process_csv(user_input: Iterable[UserInput], is_attacker: bool) -> List[Unit]:
    result = []

    for single_input in user_input:
        if single_input.unit_id:
            static = static_unit_by_id(single_input.unit_id)
        else:
            static = static_unit_by_name(single_input.name)

        if not static.meta_type == UnitMetaType.REGULAR:
            raise Exception('Unit %s/%s unsupported' % (static.id, static.jmeno))

        if single_input.count is not None:
            count = single_input.count
        else:
            raise Exception("Unsupported input, count not specified")

        # defenders never choose targeting
        unit = single_unit(static,
                           single_input.experience,
                           count,
                           is_attacker,
                           1.1 if is_attacker else 1.0,
                           targeting_override=single_input.targeting if is_attacker else None)

        result.append(unit)

    return result


class ValidationException(Exception):
    pass


def validate_submission(tournament: Tournament, submission: TournamentSubmission, units: List[Unit]):
    total_power = 0
    biggest_power = 0
    recruit_tu = 0
    biggest_unit = "N/A"
    for unit in units:
        power = unit.pocet * unit.power
        if power > biggest_power:
            biggest_power = power
            biggest_unit = '%s [%dk]' % (unit.nazev, biggest_power / 1000)
        total_power += power

    if tournament.validate_max_turns is not None:
        for unit in units:
            full_info = unit_full_info_by_id(unit.id_jedn)
            if full_info.total_recruit_tu is not None:
                recruit_tu += math.ceil(unit.pocet / full_info.total_recruit_tu)
            else:
                raise ValidationException('Unit without recruit while max recruit is set up.')

        if recruit_tu > tournament.validate_max_turns:
            raise ValidationException('Recruit takes %d turns, more than %d allowed.' % (recruit_tu, tournament.validate_max_turns))

    submission.army_power = total_power
    submission.main_stack = biggest_unit
    submission.recruit_tu = recruit_tu

    if tournament.validate_max_power is not None and total_power > tournament.validate_max_power:
        raise ValidationException('Too high power: %d' % total_power)

    unit_ids = []
    for unit in units:
        if unit.id_jedn in unit_ids:
            raise ValidationException('Duplicated unit: %s' % _unit_str(unit))

        unit_ids.append(unit.id_jedn)

        if unit.zkusenost < 1 or unit.zkusenost > 100:
            raise ValidationException('Illegal XP: %s' % _unit_str(unit))

        if unit.pocet < 1:
            raise ValidationException('Illegal count: %s' % _unit_str(unit))

    if tournament.validate_units_selection == Tournament.UNIT_TYPE_ONLY_CORE:
        for unit in units:
            if unit.id_jedn < 1001 or unit.id_jedn >= 6000:
                raise ValidationException('Not core: %s' % _unit_str(unit))

    if tournament.validate_units_selection == Tournament.UNIT_TYPE_SINGLE_COLOR_NO_NEUTRAL:
        color = None
        for unit in units:
            if unit.id_jedn < 1001 or unit.id_jedn >= 6000:
                raise ValidationException('Not core: %s' % _unit_str(unit))

            unit_color = unit_full_info_by_id(unit.id_jedn).color
            if color is None:
                color = unit_color

            if color != unit_color:
                raise ValidationException('Illegal color, expected %s: %s (%s)' % (color, _unit_str(unit), unit_color))

    if tournament.validate_min_xp_ranged is not None:
        for unit in units:
            if unit.zkusenost < tournament.validate_min_xp_ranged * 100 and unit.typ_ut == 'S':
                raise ValidationException('Min XP ranged: %s' % _unit_str(unit))
    if tournament.validate_max_xp_ranged is not None:
        for unit in units:
            if unit.zkusenost > tournament.validate_max_xp_ranged * 100 and unit.typ_ut == 'S':
                raise ValidationException('Max XP ranged: %s' % _unit_str(unit))

    if tournament.validate_min_xp_melee is not None:
        for unit in units:
            if unit.zkusenost < tournament.validate_min_xp_melee * 100 and unit.typ_ut == 'B':
                raise ValidationException('Min XP melee: %s' % _unit_str(unit))
    if tournament.validate_max_xp_melee is not None:
        for unit in units:
            if unit.zkusenost > tournament.validate_max_xp_melee * 100 and unit.typ_ut == 'B':
                raise ValidationException('Max XP melee: %s' % _unit_str(unit))

    if tournament.validate_max_units is not None:
        if len(units) > tournament.validate_max_units:
            raise ValidationException('Too many units: %d' % len(units))


def _unit_str(unit: Unit) -> str:
    return '%d x %s#%d (%.2f%%)' % (unit.pocet, unit.nazev, unit.id_jedn, unit.zkusenost)


def parse_csv(data: str) -> List[UserInput]:
    result = []

    lines = data.strip().split('\n')

    for line in lines:
        if not line:
            continue
        parts = line.split(";")
        parts = list(part.strip() for part in parts)

        targeting = None
        if len(parts) > 3:
            targeting = parts[3]
            if parts[3] not in ('pocet', 'dmg', 'zivoty', 'iniciativa'):
                raise ValidationException('Unknown targetting %s' % targeting)

        result.append(UserInput(name=parts[0],
                                experience=float(parts[1]),
                                count=int(parts[2]),
                                targeting=targeting))

    return result
